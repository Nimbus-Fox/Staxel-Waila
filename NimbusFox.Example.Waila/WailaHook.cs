﻿using Microsoft.Xna.Framework;
using NimbusFox.Example.Waila.Classes;
using NimbusFox.FoxCore.V3;
using NimbusFox.FoxCore.V3.UI.Classes;
using NimbusFox.FoxCore.V3.UI.Enums;
using Plukit.Base;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Modding;
using Staxel.Tiles;

namespace NimbusFox.Example.Waila {
    public class WailaHook : IModHookV3 {

        public WailaHook() {
        }

        public void Dispose() { }
        public void GameContextInitializeInit() {
            FoxUIHook.Instance.LoadUIContent += (graphics) => {
                var window = new UiWindow(UiAlignment.BottomCenter);

                window.AddChild(new UiBlockDisplay());

                window.Container.SetBackground(Constants.Backgrounds.Dark);

                Logger.WriteLine("Waila now loaded");
            };
        }
        public void GameContextInitializeBefore() { }
        public void GameContextInitializeAfter() { }
        public void GameContextDeinitialize() { }
        public void GameContextReloadBefore() { }
        public void GameContextReloadAfter() { }
        public void UniverseUpdateBefore(Universe universe, Timestep step) { }
        public void UniverseUpdateAfter() { }
        public bool CanPlaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            return true;
        }

        public bool CanReplaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            return true;
        }

        public bool CanRemoveTile(Entity entity, Vector3I location, TileAccessFlags accessFlags) {
            return true;
        }

        public void ClientContextInitializeInit() { }
        public void ClientContextInitializeBefore() { }
        public void ClientContextInitializeAfter() { }
        public void ClientContextDeinitialize() { }
        public void ClientContextReloadBefore() { }
        public void ClientContextReloadAfter() { }
        public void CleanupOldSession() { }
        public bool CanInteractWithTile(Entity entity, Vector3F location, Tile tile) {
            return true;
        }

        public bool CanInteractWithEntity(Entity entity, Entity lookingAtEntity) {
            return true;
        }
    }
}
