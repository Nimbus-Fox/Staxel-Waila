﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using NimbusFox.FoxCore.V3.UI.Classes;
using Plukit.Base;
using Staxel;
using Staxel.Client;
using Staxel.Input;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Tiles;

namespace NimbusFox.Example.Waila.Classes {
    public sealed class UiBlockDisplay : UiElement {
        private UiTextBlock _blockName;
        private UiTextBlock _modName;
        private UiTextBlock _modAuthor;

        internal static ScanCode ToggleKey = ScanCode.H;

        private bool _toggle = true;

        public UiBlockDisplay() {
            _blockName = new UiTextBlock();
            _modName = new UiTextBlock();
            _modAuthor = new UiTextBlock();
            AddChild(_blockName);
            AddChild(_modName);
        }

        private static Dictionary<string, string> Cache { get; } = new Dictionary<string, string>();

        /// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
        public override void Dispose() { }

        public override void Update(Universe universe, Vector2 origin, AvatarController avatar, List<ScanCode> input, bool ctrl, bool shift,
            IReadOnlyList<InterfaceLogicalButton> inputPressed, Vector2 mouseLocation, bool click, bool clickHold) {
            Window.Hide();

            if (input.Any()) {
                if (input.Contains(ToggleKey)) {
                    _toggle = !_toggle;
                }
            }

            if (!_toggle) {
                return;
            }

            var players = new Lyst<Entity>();
            universe.GetPlayers(players);
            foreach (var player in players) {
                if (ClientContext.PlayerFacade.IsLocalPlayer(player)) {
                    if (player.PlayerEntityLogic.LookingAtTile(out var target, out _)) {
                        Window.Show();
                        if (universe.ReadTile(target, TileAccessFlags.None, out var tile)) {
                            if (tile.Configuration.Code == Staxel.Core.Constants.CompoundCode) {
                                if (universe.FindReadCompoundTileCore(target, TileAccessFlags.None, out _,
                                    out var compoundTile)) {
                                    SetText(compoundTile.Configuration);
                                    break;
                                }
                            }
                            SetText(tile.Configuration);
                            break;
                        }
                    }
                }
            }
        }

        private void SetText(TileConfiguration config) {
            _blockName.SetString(ClientContext.LanguageDatabase.GetTranslationString(config.Code + ".name"));
            if (config.Source.StartsWith("staxel")) {
                _modName.SetString("Staxel");
                _modName.SetColor(Color.ForestGreen);
                if (Elements.Contains(_modAuthor)) {
                    Elements.Remove(_modAuthor);
                }
                return;
            }
            _modName.SetColor(Color.White);

            if (!Elements.Contains(_modAuthor)) {
                AddChild(_modAuthor);
            }

            var mod = config.Source;
            mod = mod.Substring(5);
            mod = mod.Substring(0, mod.IndexOf('/'));

            string author;
            if (!Cache.ContainsKey(mod)) {
                var stream = GameContext.ContentLoader.ReadStream("mods/" + mod + ".manifest");
                stream.Seek(0L, SeekOrigin.Begin);
                var blob = BlobAllocator.Blob(false);
                blob.ReadJson(stream.ReadAllText());
                stream.Dispose();
                author = blob.GetString("author");
                Cache.Add(mod, author);
            }

            author = Cache[mod];
            _modAuthor.SetColor(author == "NimbusFox" ? Color.Orange : Color.CornflowerBlue);
            _modAuthor.SetString(author);
            _modName.SetString(mod);
        }
    }
}
